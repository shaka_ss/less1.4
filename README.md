```
root@jw:~# docker run -v $(pwd)/nginx.conf:/opt/nginx/conf/nginx.conf -d -p 8068:80 9060ac60fbdd
9aa2e0c2dfa0e611a4caae2a956c86015100f8b40e094350420b2f930d4ad7aa

root@jw:~# curl http://127.0.0.1:8068/hello/
Hello world by lua!

root@jw:~# curl http://127.0.0.1:8068/
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```
