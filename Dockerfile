FROM debian:9 as nginxdirty

RUN apt update && apt install -y wget make gcc g++ file git
WORKDIR /opt
RUN wget -q https://github.com/openresty/luajit2/archive/v2.1-20201027.tar.gz && tar xvfz v2.1-20201027.tar.gz
WORKDIR /opt/luajit2-2.1-20201027
RUN make && make install
WORKDIR /opt
RUN wget -q https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && tar xvfz v0.3.1.tar.gz
RUN wget -q https://github.com/openresty/lua-nginx-module/archive/v0.10.19.tar.gz && tar xvfz v0.10.19.tar.gz
RUN wget -q https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz && tar xvfz pcre-8.44.tar.gz
RUN wget -q https://zlib.net/zlib-1.2.11.tar.gz && tar xvfz zlib-1.2.11.tar.gz
WORKDIR /opt/zlib-1.2.11
RUN ./configure && make && make install
WORKDIR /opt
RUN git clone https://github.com/openresty/lua-resty-core.git
RUN wget -q http://nginx.org/download/nginx-1.19.3.tar.gz && tar xvfz nginx-1.19.3.tar.gz
WORKDIR /opt/nginx-1.19.3
RUN export LUAJIT_LIB=/usr/local/lib/libluajit-5.1.so && \
export LUAJIT_INC=/usr/local/include/luajit-2.1 && \
./configure --prefix=/opt/nginx \
         --with-pcre=/opt/pcre-8.44 \
         --with-ld-opt="-Wl,-rpath,/usr/local/lib/libluajit-5.1.so" \
         --add-module=/opt/ngx_devel_kit-0.3.1 \
         --add-module=/opt/lua-nginx-module-0.10.19
RUN make && make install

FROM debian:9 as nginxclean
WORKDIR /opt/nginx/sbin
COPY --from=nginxdirty /usr/local/bin/luajit /usr/local/bin/luajit
COPY --from=nginxdirty  /usr/local/lib/libluajit-5.1.so.2.1.0 /lib/libluajit-5.1.so.2
COPY --from=nginxdirty /opt/nginx /opt/nginx
RUN mkdir /opt/lua-resty-core
COPY --from=nginxdirty /opt/lua-resty-core/lib /opt/lua-resty-core/
RUN sed '14,17d' /opt/lua-resty-core/resty/core/regex.lua > /opt/lua-resty-core/resty/core/regex.lua
CMD ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]

